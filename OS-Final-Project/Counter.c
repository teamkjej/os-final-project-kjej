
// Concurrent-Counter

//Standard Libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#include <pthread.h>
#define Nthreads 2

typedef struct counter_t{
  int value;
  pthread_mutex_t lock;
} counter_t;

void init(counter_t *c){
  c->value = 0;
  pthread_mutex_init(&c->lock, NULL);
}

void *increment(void *data){
  counter_t *c;
  c  =  (counter_t *) data;
  pthread_mutex_lock(&c->lock);
  c->value++;
  pthread_mutex_unlock(&c->lock);

  return NULL;
}

void *increment_nolock(void *data){
  counter_t *d;
  d  =  (counter_t *) data;
  d->value++;
  return NULL;
}

void *decrement(void *data){
  counter_t *c;
  c = (counter_t *) data;
  pthread_mutex_lock(&c->lock);
  c->value--;
  pthread_mutex_unlock(&c->lock);
  return NULL;
}

void *decrement_nolock(void *data){
  counter_t *c;
  c = (counter_t *) data;
  c->value--;
  return NULL;
}

int get(counter_t *c){
  pthread_mutex_lock(&c->lock);
  int rc = c->value;
  pthread_mutex_unlock(&c->lock);
  return rc;
}

int CounterTest () {
  /* code */
  //Testing the Concurrent counter

  //Creating the counter object
  pthread_t thread[Nthreads], thread_two[Nthreads];
  counter_t c;
  counter_t d;
  //Initializing the counter value and the lock
  init(&c);
  init(&d);

  printf("\n\nWe are in the main code, Begin Now: \n\n");

  //10,000 iterations with two threads
  for(int j = 0; j < 10000; j++){
    for(int i=0; i < Nthreads; i++){
      pthread_create(&thread[i], NULL, increment, (void *)&c);
      (void)pthread_join(thread[i], NULL);
    }
  }

  //10,000 iterations with two threads
  //Non concurrent counter
  for(int a = 0; a < 10000; a++){
    for(int b=0; b < Nthreads; b++){
      pthread_create(&thread_two[b], NULL, increment_nolock, (void *)&d);
    }
  }

  //Print statement with the final value of the counter
  printf("We are after 10,000 iterations with two threads updating the counter.\n");
  printf("We have the counter value to be: %d\n", c.value);

  //Print statement with the final value of the counter
  printf("\nWe are after 10,000 iterations with two threads updating the counter, without locks.\n");
  printf("We have the counter value to be: %d\n\n", d.value);

  return 0;
}
