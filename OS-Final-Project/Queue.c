#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <pthread.h>
#include <assert.h>

typedef struct __node_t {
    int value;
    struct __node_t *next;
}

node_t;

typedef struct __queue_t {
    node_t *head;
    node_t *tail;
    pthread_mutex_t headLock;
    pthread_mutex_t tailLock;
    //pthread_cond_t cond;
}
queue_t;

void Queue_Init(queue_t *q) {

    node_t *tmp = malloc(sizeof(node_t));
    tmp->next = NULL;
    q->head = q->tail = tmp;
    pthread_mutex_init(&q->headLock, NULL);
    pthread_mutex_init(&q->tailLock, NULL);

}

void Queue_Enqueue(queue_t *q, int value) {
    node_t *tmp = malloc(sizeof(node_t));
    assert(tmp != NULL);
    tmp->value = value;
    tmp->next  = NULL;

    pthread_mutex_lock(&q->tailLock);
    q->tail->next = tmp;
    q->tail = tmp;
    pthread_mutex_unlock(&q->tailLock);

}


int Queue_Delete(queue_t *q) {
    pthread_mutex_lock(&q->headLock);
    node_t *tmp = q->head;
    node_t *newHead = tmp->next;
    if (newHead == NULL) {
        pthread_mutex_unlock(&q->headLock);
        return -1; // queue was empty
}
    q->head = newHead;
    pthread_mutex_unlock(&q->headLock);
    free(tmp);
    return newHead->value;
}




void  *enqueue (void *data ) {
    queue_t * queue;
    queue  =  (queue_t *) data;

    for(int i = 1; i<=49;i++) {
       Queue_Enqueue(queue,i);
   }

  return NULL;
}
void  *enqueue2 (void *data ) {
    queue_t * queue;
    queue  =  (queue_t *) data;
    int len = 100;
    for(int i = 50; i<=len;i++) {
       Queue_Enqueue(queue,i);
   }

  return NULL;
}


void  *dequeue (void *data) {
    queue_t  * queue;
    queue  =  (queue_t *)data;
    int len = 100;
    for(int i = 1; i<=len;i++) {

       printf("%d ",Queue_Delete(queue));
       printf("%s",", ");

   }

    return NULL;
}

int QueueTest() {

    queue_t  myqueue;
    Queue_Init(&myqueue);
    int len = 100;

    pthread_t thread1;
    pthread_t thread2;


   pthread_create(&thread1, NULL, enqueue,(void *)&myqueue);
   pthread_create(&thread2, NULL, enqueue2, (void *)&myqueue);
   //pthread_create(&thread3, NULL, dequeue, (void *)&myqueue);


    (void)pthread_join(thread1, NULL);
    (void)pthread_join(thread2, NULL);
   // (void)pthread_join(thread3, NULL);

   for(int i = 1; i<=len;i++) {

     printf("%d",Queue_Delete(&myqueue));
     printf("%s",", ");

   }
    // printf("it worked");
        return 0;
}
