// #include "LinkedList.c"
#include "Counter.c"
#include "Hash.c"
// #include "Queue.c"

// NB: QUEUE IS TESTED IN QueueTest.c


// This is the main Function to Run all Test Files execept Queue
int main(){
  printf("%s\n"," ----------------> This is a a Test of the Concurrent Counter <---------------" );
  printf("%s\n"," " );
  CounterTest ();
  printf("%s\n"," " );
  printf("%s\n"," " );
  printf("%s\n"," " );

  printf("%s\n"," ----------------> This is a a Test of the Concurrent Hash <---------------" );
  printf("%s\n"," " );
  HashTest();
  printf("%s\n"," " );
  printf("%s\n"," " );
  printf("%s\n"," " );
  printf("%s\n"," " );
  printf("%s\n"," " );



  printf("%s\n"," ----------------> This is a a Test of the Concurrent LinkedList <---------------" );
  printf("%s\n"," " );

   // LinkedListTest();
  LinkedListTest();
  printf("%s\n"," " );






}
