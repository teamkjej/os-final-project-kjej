// STANDARD LIBRARIES
#include <stdio.h> // Standard Input and Output Operations
#include <string.h> // Functions to manipulate C strings and arrays
#include <stdlib.h> //Four variable types, several macros, and various functions
#include <sys/wait.h> //Symbolic constants for use with waitpid():
#include <unistd.h> // Miscellaneous symbolic constants and types,
#include <pthread.h> // Include Thread


// basic node structure
typedef struct __node_t {
  int key;
  struct __node_t *next;
} node_t;

// basic list structure (one used per list)
typedef struct __list_t {
  node_t *head;
  pthread_mutex_t lock;
} list_t;

// Define List Structure
void List_Init(list_t *L) {
  L->head = NULL;
  pthread_mutex_init(&L->lock, NULL);
}

// Insert Function
int List_Insert(list_t *L, int key) {
  pthread_mutex_lock(&L->lock);
  node_t *new = malloc(sizeof(node_t));
  if (new == NULL) {
      perror("malloc");
      pthread_mutex_unlock(&L->lock);
      return -1; // fail
  }
  new->key  = key;
  new->next = L->head;
  L->head   = new;
  pthread_mutex_unlock(&L->lock);
  return 0; // success


}

// Searching in List
int List_Lookup(list_t *L, int key) {
  int rv = -1;
  pthread_mutex_lock(&L->lock);
  node_t *curr = L->head;
  while (curr) {
    if (curr->key == key) {
      rv = 0;
      break;
    }
      curr = curr->next;
  }
  pthread_mutex_unlock(&L->lock);
  return rv; // now both success and failure

}

// Deleting from a List
void List_Delete(list_t *L, struct __node_t *headref, int key){

  pthread_mutex_lock(&L->lock);
  struct __node_t *temp = headref, *prev;

  if (temp != NULL && temp->key == key) {
    headref = temp->next;
    // free(temp);
    pthread_mutex_unlock(&L->lock);
    return;
  }

  while (temp != NULL && temp->key != key){
      prev = temp;
      temp = temp->next;
  }

  if (temp == NULL){
    pthread_mutex_unlock(&L->lock);
    return;
  }

  // Unlink the node from linked list
  prev->next = temp->next;
  // free(temp);
  pthread_mutex_unlock(&L->lock);
  return;

}


void printList(struct __list_t *list)
{
    node_t *curr = list->head;
    while (curr) {
      printf(" %d ", curr->key);
      printf(" %s ", "->");

      curr = curr->next;
    }
}

void *List_Insert_Threads(void *data){
  list_t * linkedList;
  linkedList  =  (list_t *) data;
  int len = 20;
  for(int i = 1; i<=len;i++) {
     List_Insert(linkedList,i);
  }
  return NULL;
}
//
// void *List_Delete_Threads(void *data){
//   list_t * linkedList;
//   linkedList  =  (list_t *) data;
//   int len = 20;
//   for(int i = 1; i<=len;i++) {
//      printf("%d \n",List_Delete(linkedList, linkedList->head, i));
//   }
//   return NULL;
// }

int TestWithoutThreads(){
  printf("%s\n","----> Testing LinkedList Without Multiple Threads " );
  printf("%s\n","----> Creating Instance of LinkedList" );
  list_t linkedList;

  printf("%s\n","----> Initializing LinkedList" );
  List_Init(&linkedList);

  printf("%s\n","----> Inserting into The Linked List" );
  List_Insert(&linkedList, 1);
  List_Insert(&linkedList, 2);
  List_Insert(&linkedList, 3);
  List_Insert(&linkedList, 4);
  List_Insert(&linkedList, 5);

  printf("%s\n","----> Looking up For a Member" );
  int lookUp;
  lookUp = List_Lookup(&linkedList, 2);
  printf("%d\n",lookUp);

  printf("%s\n","----> Printing Members of the List" );
  printList(&linkedList);

  printf("%s\n","" );
  printf("%s\n","----> Deleting a Node" );
  List_Delete(&linkedList, linkedList.head, 3);
  printList(&linkedList);
  printf("%s\n","" );

  return 0;
}

int TestWithThreads(){
  pthread_t thread1;
  pthread_t thread2;

  printf("%s\n","----> Testing LinkedList With Multiple Threads Coccurently  " );
  printf("%s\n","----> Creating Instance of LinkedList" );
  list_t linkedList;

  printf("%s\n","----> Initializing LinkedList" );
  List_Init(&linkedList);

  pthread_create(&thread1, NULL, List_Insert_Threads,(void *)&linkedList);
  pthread_create(&thread2, NULL,  List_Insert_Threads,(void *)&linkedList);

  (void)pthread_join(thread1, NULL);
  (void)pthread_join(thread2, NULL);

  printList(&linkedList);
  printf("%s\n","" );

  return 0;
}

 int LinkedListTest()
 {

  TestWithoutThreads();
  TestWithThreads();
  return 0;


}
