
#define BUCKETS (101)
#include "LinkedList.c"

typedef struct __hash_t {
    list_t lists[BUCKETS];
    } hash_t;

void Hash_Init(hash_t *H){
    int i;
    for (i = 0; i < BUCKETS; i++){
        List_Init(&H->lists[i]);
    }
}

int Hash_Insert(hash_t *H, int key){
    int bucket = key % BUCKETS;
    return List_Insert(&H->lists[bucket], key);
}


int Hash_Lookup(hash_t * H, int key){
    int bucket = key % BUCKETS;
    return List_Lookup(&H->lists[bucket], key);
}

void Hash_Delete(hash_t *H, int key){
    int bucket = key % BUCKETS;
    // List_Delete(&H->lists[bucket],H->lists[bucket].head, key);
    H->lists[bucket].head = NULL;
}

void printHash(hash_t *hash){
    int i;
    for (i = 0; i< BUCKETS; i++){
    printList(&hash->lists[i]);
    }
}

void *Hash_Insert_Threads(void *data)
{
    hash_t *hashj;
    hashj = (hash_t *)data;
    int len = 5;
    for (int i = 1; i <= len; i++)
    {
        Hash_Insert(hashj, i);
    }
    return NULL;
}

// Test Code
int HashTest(){
    pthread_t t1;
    pthread_t t2;

    hash_t hashj;

    Hash_Init(&hashj);



    pthread_create(&t1, NULL, Hash_Insert_Threads, (void *)&hashj);
    pthread_create(&t2, NULL, Hash_Insert_Threads, (void *)&hashj);

    (void)pthread_join(t1, NULL);
    (void)pthread_join(t2, NULL);

    printHash(&hashj);
    return 0;
}
